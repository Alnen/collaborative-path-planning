//
//  Point2D.h
//  Boost xml parser
//
//  Created by Alnen on 02.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Point2D_h
#define Point2D_h

#include <math.h>
#include <utility>
#include <algorithm>

using std::is_integral;
using std::is_floating_point;
using std::enable_if;
using std::common_type;


template <typename T>
class Point2D
{
    static_assert(is_integral<T>::value || is_floating_point<T>::value,"");
    
private:
    T x_;
    T y_;
    
public:
    Point2D(T x = T{0},T y = T{0}):x_(x),y_(y){}
    Point2D(T constant): Point2D(constant,constant){}
    
    Point2D(const Point2D& other) = default;     //copy

    
    T x() const { return x_; }
    T y() const { return y_; }
    
    void set_x(T x) { x_ = x;}
    void set_y(T y) { y_ = y;}
    
    T norm() const {
        return sqrt(x_ * x_ + y_ * y_);
    }
    
    template <class V>
    Point2D<T> rotate(V angle) const {
        static_assert(is_floating_point<V>::value,"Angle must be of floating point type");
        
        T sin_theta = sin(angle);
        T cos_theta = cos(angle);
        
        return Point2D<T>{ x_*cos_theta - y_*sin_theta, x_*sin_theta + y_*cos_theta };
    }
    
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T> operator+(V modificator) const
    {
        return (Point2D<T>(this) += modificator);
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T> operator-(V modificator) const
    {
        return (Point2D<T>(this) -= modificator);
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T> operator*(V modificator) const
    {
        return (Point2D<T>(this) *= modificator);
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T> operator/(V modificator) const
    {
        return (Point2D<T>(this) /= modificator);
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator+=(V modificator)
    {
        x_ += modificator;
        y_ += modificator;
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator-=(V modificator)
    {
        x_ -= modificator;
        y_ -= modificator;
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator*=(V modificator)
    {
        x_ *= modificator;
        y_ *= modificator;
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator/=(V modificator)
    {
        x_ /= modificator;
        y_ /= modificator;
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator=(V modificator)
    {
        x_ = modificator;
        y_ = modificator;
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator=(const Point2D<V>& other)
    {
        x_ = other.x();
        y_ = other.y();
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator+=(const Point2D<V>& other)
    {
        x_ += other.x();
        y_ += other.y();
        return *this;
    }
    
    template<class V, typename = typename enable_if<is_integral<V>::value || is_floating_point<V>::value>::type>
    Point2D<T>& operator-=(const Point2D<V>& other)
    {
        x_ -= other.x();
        y_ -= other.y();
        return *this;
    }
};

template <class T, class V>
Point2D<typename common_type<T,V>::type> operator+(Point2D<T> point1, Point2D<V> point2){
    return Point2D<typename common_type<T,V>::type>{ point1.x()+point2.x(), point1.y()+point2.y()};
}

template <class T, class V>
Point2D<typename common_type<T,V>::type> operator-(Point2D<T> point1, Point2D<V> point2){
    return Point2D<typename common_type<T,V>::type>{ point1.x()-point2.x(), point1.y()-point2.y()};
}

template <class T, class V, typename = typename enable_if<is_integral<T>::value || is_floating_point<T>::value>::type>
Point2D<V> operator+(T modificator, Point2D<V> point2){
    return (Point2D<V>(point2) += modificator);
}

template <class T, class V, typename = typename enable_if<is_integral<T>::value || is_floating_point<T>::value>::type>
Point2D<V> operator-(T modificator, Point2D<V> point2){
    return (Point2D<V>(point2) -= modificator);
}

template <class T, class V, typename = typename enable_if<is_integral<T>::value || is_floating_point<T>::value>::type>
Point2D<V> operator*(T modificator, Point2D<V> point2){
    return (Point2D<V>(point2) *= modificator);
}
template <class P>
using Vector2D = Point2D<P>;

#endif
