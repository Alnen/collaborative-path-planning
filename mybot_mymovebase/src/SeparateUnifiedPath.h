//
//  SeparateUnifiedPath.h
//  Boost xml parser
//
//  Created by Alnen on 07.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_SeparateUnifiedPath_h
#define Boost_xml_parser_SeparateUnifiedPath_h


#include <vector>
#include <utility>

#include "MapConfiguration.h"
#include "Point2D.h"


using std::pair;
using std::vector;


template <class P>
class SeparateUnifiedPath
{
private:
    P distance_;
    
public:
    using Point = Point2D<P>;
    using Vector = Vector2D<P>;
    using Pair = pair<vector<Point>, vector<Point>>;
    
    SeparateUnifiedPath (P distance) :distance_(distance) {}
    
    Pair operator()(const vector<MapConfiguration<P>>& unified_path) {
        vector<Point> first_robot_path,second_robot_path;
        
        for (const auto& configuration: unified_path) {
            first_robot_path.push_back(configuration.location());
            second_robot_path.push_back(configuration.location() + Point{distance_,0.0}.rotate(configuration.angle()));
        }
        
        return move(Pair{std::move(first_robot_path),std::move(second_robot_path)});
    }
};

#endif
