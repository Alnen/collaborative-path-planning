//
//  Algorithm.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_PathPlanner_h
#define Boost_xml_parser_PathPlanner_h

#include <deque>
#include <fstream>
#include <vector>


#include "ConfigurationSpaceConfiguration.h"
#include "ConfigurationSpaceMapBase.h"
#include "Footprint.h"
#include "Map.h"
#include "MapConfiguration.h"
#include "Resolution.h"

#define  OBSTACLE_VALUE 1
#define  COLISION_VALUE 1


template <class CSP = int, class MP = double, class FP = double, class Container = std::vector<Footprint<FP>>>
class PathPlanner
{
    static_assert(is_integral<CSP>::value,"CSP must be of integral type");
    
    using CSMap = ConfigurationSpaceMapBase<CSP>;
    using Point = Point2D<FP>;
    using Vector = Point;
    using index_type = CSP;
    
private:
    template <class T>
    using CSConfiguration = ConfigurationSpaceConfiguration<T>;
    
private:
    Container obstacles_;
    CSMap configuration_space_;
    Map<MP>   map_;
    Resolution<MP> resolution_;

    
    typename CSMap::index_type get_x_index (MP map_x_position) const {
        return (map_x_position - map_.lx())/resolution_.linear_resolution();
    }
    
    MP get_x_position (typename CSMap::index_type cs_x_index) const {
        return cs_x_index*resolution_.linear_resolution() +  map_.lx();
    }
    
    
    typename CSMap::index_type get_y_index (MP map_y_position) const {
        return configuration_space_.height() - (map_y_position - map_.ly())/resolution_.linear_resolution();
    }
    
    MP get_y_position (typename CSMap::index_type cs_y_index) const {
        return (configuration_space_.height() - cs_y_index)*resolution_.linear_resolution() +  map_.ly();
    }
    
    /* 
     *      angle >= 0.0 && angle <= 2.0*M_PI
     */
    typename CSMap::index_type get_z_index (MP angle) const {
        return angle/resolution_.angular_resolution();
    }
    
    MP get_angle_from_z_index (typename CSMap::index_type cs_z_index) const {
        return cs_z_index*resolution_.angular_resolution();
    }
    
    MP get_angle_from_3_points (const Point& p1, const Point& p2, const Point& p3) {
        Vector ab = { p2.x() - p1.x(), p2.y() - p1.y() };
        Vector cb = { p2.x() - p3.x(), p2.y() - p3.y() };
        
        MP dot = (ab.x() * cb.x() + ab.y() * cb.y());
        MP cross = (ab.x() * cb.y() - ab.y() * cb.x());
        
        MP alpha = atan2(cross, dot);
        
        return alpha;
    }
    
    bool fuzzy_compare(MP A, MP B){
        static const MP EPSILON = 0.1;
        MP diff = A - B;
        return (diff < EPSILON) && (-diff < EPSILON);
    }
    
    //TODO исправить баг с y координатой и проверить z на 
    void fill_map (const Footprint<FP>& robot_footprint) {
        static_assert(std::is_same<
                      typename Footprint<FP>::CollisionForm,
                      SquareCollision<typename Footprint<FP>::Precision>
                                  >::value,
                      "Only rectangle collision supported");
        
        auto left_index   = get_x_index(robot_footprint.collision_form().left()),
             bottom_index = get_y_index(robot_footprint.collision_form().bottom()),
             right_index  = get_x_index(robot_footprint.collision_form().right()),
             top_index    = get_y_index(robot_footprint.collision_form().top());
        
        if (left_index   < 0) left_index = 0;
        if (right_index  >= configuration_space_.width()) right_index = configuration_space_.width() - 1;
        if (bottom_index < 0) bottom_index = 0;
        if (top_index    >= configuration_space_.height()) top_index = configuration_space_.height() - 1;
        
        
        /*auto left_index1   = get_x_index(map_.lx()),
             bottom_index1 = get_y_index(map_.ly()),
             right_index1  = get_x_index(map_.lx()+map_.width()),
             top_index1    = get_y_index(map_.ly()+map_.height());
        
        auto left_index2   = configuration_space_.width(),
             bottom_index2 = configuration_space_.height();
        
        auto left_index3   = get_x_position(configuration_space_.width()),
             bottom_index3 = get_y_position(configuration_space_.height());*/
        
        
        for (auto x = left_index; x <= right_index; ++x) {
            for (auto y = top_index; y <= bottom_index; ++y) {
                if (configuration_space_.get(x,y,0) != 1) {
                    Point current_point{get_x_position(x),get_y_position(y)};
                    
                    const Point *p1 = &*robot_footprint.points().begin(),
                                *p2 = nullptr;
                    
                    MP angle = -100.0,
                       sum_angle = 0.0;
                    
                    bool flip = false,
                         inside = false;
                    
                    for (auto it = ++robot_footprint.points().begin(); it != robot_footprint.points().end(); ++it) {
                        if (flip){
                            p1 = &*it;
                            angle = get_angle_from_3_points(*p2, current_point, *p1);
                        } else {
                            p2 = &*it;
                            angle = get_angle_from_3_points(*p1, current_point, *p2);
                        }
                        
                        if (fuzzy_compare(abs(angle), M_PI)) {
                            inside = true;
                            break;
                        }
                        sum_angle += angle;
                        flip = !flip;
                    }
                    if(!inside){
                        if (flip){
                            p1 = &*robot_footprint.points().begin();
                            angle = get_angle_from_3_points(*p2, current_point, *p1);
                        } else {
                            p2 = &*robot_footprint.points().begin();
                            angle = get_angle_from_3_points(*p1, current_point, *p2);
                        }
                        sum_angle += angle;
                        
                        if (fuzzy_compare(fabs(sum_angle), 2.0*M_PI))
                            inside = true;
                    }
                    if (inside) {
                        for (typename CSMap::index_type z = 0; z < configuration_space_.depth(); ++z) {
                            configuration_space_.set(x,y,z,1);
                        }
                    }
                }
            }
        }
    }
    
public:
    PathPlanner(const Container&& obstacles, const Map<MP>& map,const Resolution<MP>& resolution,const Footprint<FP>& robot_footprint)
    :obstacles_(obstacles),
     configuration_space_(static_cast<int>(map.width()/resolution.linear_resolution() + 1),
                          static_cast<int>(map.height()/resolution.linear_resolution() + 1),
                          static_cast<int>(2*M_PI/resolution.angular_resolution())),
     map_(map),
     resolution_(resolution)
    {
        for (const Footprint<FP>& robot_footprint : obstacles_) {
            fill_map(robot_footprint);
        }
        
        //
        
        Footprint<FP> rotated_robot_footprint = robot_footprint;
        //Ппроверка что робот влезает
        for (auto z = 0; z < configuration_space_.depth(); ++z) {
            //std:: cout << get_angle_from_z_index(z) << std::endl;
            if (z != 0) {
                rotated_robot_footprint = robot_footprint.rotate(get_angle_from_z_index(z));
            }
            
            for (auto y = 0; y < configuration_space_.height() ; ++y) {
                for (auto x = 0; x < configuration_space_.width(); ++x) {
                    if (configuration_space_.get(x,y,z) == 0) {
                        const Point current_location { get_x_position(x), get_y_position(y)};
                        const Footprint<FP> current_footprint = rotated_robot_footprint.translate(current_location);
                        bool overlapped = false;
                        for (const auto& obstacle : obstacles_) {
                            if(current_footprint.isColision(obstacle)){
                                overlapped = true;
                                break;
                            }
                        }
                        if (overlapped) {
                            configuration_space_.set( x, y, z, COLISION_VALUE );
                        }
                    }
                }
            }
        }
        
        /*
        std::fstream fout("/Users/alnen/Library/Developer/Xcode/DerivedData/Boost_xml_parser-gyykeaieeyvzcxajskhswnimrjhy/Build/Products/Debug/out.txt", std::ofstream::out);
        for (typename CSMap::index_type z = 0; z < configuration_space_.depth(); ++z) {
            fout <<"Angle:"<< z << std::endl;
            for (typename CSMap::index_type y = 0; y < configuration_space_.height(); ++y) {
                for (typename CSMap::index_type x = 0; x < configuration_space_.width(); ++x) {
                    fout << configuration_space_.get(x,y,z) << " ";
                }
                fout << std::endl;
            }
            fout << std::endl << std::endl;
        }
        fout.close();
         */
        
    }
    
    std::vector<MapConfiguration<MP>> getPath (const MapConfiguration<MP>& current_configuration,
                                               const MapConfiguration<MP>& goal_configuration)
    {
        //достать индекс  x y z у текущей и цели
        auto current_x_index = get_x_index(current_configuration.location().x()),
             current_y_index = get_y_index(current_configuration.location().y()),
             current_z_index = get_z_index(current_configuration.angle()),
             goal_x_index = get_x_index(goal_configuration.location().x()),
             goal_y_index = get_y_index(goal_configuration.location().y()),
             goal_z_index = get_z_index(goal_configuration.angle());
        //проверить что конфигурации допустимы
        if (current_x_index<0 || current_x_index >= configuration_space_.width() ||
            current_y_index<0 || current_y_index >= configuration_space_.height()||
            current_z_index<0 || current_z_index >= configuration_space_.depth() ||
            configuration_space_.get(current_x_index,current_y_index,current_z_index) == 1 ||
            goal_x_index<0 || goal_x_index >= configuration_space_.width()       ||
            goal_y_index<0 || goal_y_index >= configuration_space_.height()      ||
            goal_z_index<0 || goal_z_index >= configuration_space_.depth()       ||
            configuration_space_.get(goal_x_index,goal_y_index,goal_z_index) == 1
            )
        {
            return move(std::vector<MapConfiguration<MP>>());
        }
        //Если ок запустить машину смерти
        
        std::deque<CSConfiguration<index_type>> bfs_deque; // стек для обхода в ширину
        CSMap configuration_space = configuration_space_;// скопируем пространство состояний
        bool found = false;
        
        configuration_space.set(current_x_index,current_y_index,current_z_index,2);//значение в корне
        bfs_deque.emplace_back(current_x_index,current_y_index,current_z_index);
        while (!bfs_deque.empty()) {
            const auto& current_configuration_space_configuration = bfs_deque.front();
            
            auto x = current_configuration_space_configuration.x(),
                 y = current_configuration_space_configuration.y(),
                 z = current_configuration_space_configuration.z();
            if (x == goal_x_index &&
                y == goal_y_index &&
                z == goal_z_index)
            {
                found = true;
                break;
            }
            typename CSMap::map_value_type new_value = configuration_space.get(x,y,z) + 1;
            //аккуратно проверяем 6 соседей
            //проверка по x слева и справа
            if (x > 0) {
                //x-1 case
                if (configuration_space.get(x-1,y,z) == 0) {
                    configuration_space.set(x-1,y,z,new_value);
                    bfs_deque.emplace_back(x-1,y,z);
                }
            }
            if (x < configuration_space_.width() - 1) {
                //x+1 case
                if (configuration_space.get(x+1,y,z) == 0) {
                    configuration_space.set(x+1,y,z,new_value);
                    bfs_deque.emplace_back(x+1,y,z);
                }
            }
            
            //проверка по y снизу и сверху
            if (y > 0) {
                //y-1
                if (configuration_space.get(x,y-1,z) == 0) {
                    configuration_space.set(x,y-1,z,new_value);
                    bfs_deque.emplace_back(x,y-1,z);
                }
            }
            if (y < configuration_space_.height() - 1) {
                //y+1
                if (configuration_space.get(x,y+1,z) == 0) {
                    configuration_space.set(x,y+1,z,new_value);
                    bfs_deque.emplace_back(x,y+1,z);
                }
            }
            //проверка по z под и над
            if (z == 0) {
                // max_z
                if (configuration_space.get(x,y,configuration_space.depth()-1) == 0) {
                    configuration_space.set(x,y,configuration_space.depth()-1,new_value);
                    bfs_deque.emplace_back(x,y,configuration_space.depth()-1);
                }
            } else {
                // обычное дело
                if (configuration_space.get(x,y,z-1) == 0) {
                    configuration_space.set(x,y,z-1,new_value);
                    bfs_deque.emplace_back(x,y,z-1);
                }
            }
            
            if (z == configuration_space_.depth() - 1) {
                // 0
                if (configuration_space.get(x,y,0) == 0) {
                    configuration_space.set(x,y,0,new_value);
                    bfs_deque.emplace_back(x,y,0);
                }
            } else {
                // обычное дело
                if (configuration_space.get(x,y,z+1) == 0) {
                    configuration_space.set(x,y,z+1,new_value);
                    bfs_deque.emplace_back(x,y,z+1);
                }
            }
            bfs_deque.pop_front();
        }
        
        if(!found){
            return move(std::vector<MapConfiguration<MP>>());
        }
        
        //Алгоритм закончил работу осталось выписать
        std::vector<CSConfiguration<index_type>> answer_in_cs_domain; //ответ в пространстве конфигураций
        auto current_x = goal_x_index,
             current_y = goal_y_index,
             current_z = goal_z_index;
        
        typename CSMap::index_type search_value;
        
        while (current_x != current_x_index ||
               current_y != current_y_index ||
               current_z != current_z_index)
        {
            answer_in_cs_domain.emplace_back(current_x, current_y, current_z);
            search_value = configuration_space.get(current_x, current_y, current_z) - 1;
            if (current_x > 0) {
                //x-1 case
                if (configuration_space.get(current_x-1,current_y,current_z) == search_value) {
                    --current_x;
                    continue;
                }
            }
            if (current_x < configuration_space_.width() - 1) {
                //x+1 case
                if (configuration_space.get(current_x+1,current_y,current_z) == search_value) {
                    ++current_x;
                    continue;
                }
            }
            
            //проверка по y снизу и сверху
            if (current_y > 0) {
                //y-1
                if (configuration_space.get(current_x,current_y-1,current_z) == search_value) {
                    --current_y;
                    continue;
                }
            }
            if (current_y < configuration_space_.height() - 1) {
                //y+1
                if (configuration_space.get(current_x,current_y+1,current_z) == search_value) {
                    ++current_y;
                    continue;
                }
            }
            //проверка по z под и над
            if (current_z == 0) {
                // max_z
                if (configuration_space.get(current_x,current_y,configuration_space.depth()-1) == search_value) {
                    current_z = configuration_space.depth()-1;
                    continue;
                }
            } else {
                // обычное дело
                if (configuration_space.get(current_x,current_y,current_z-1) == search_value) {
                    --current_z;
                    continue;
                }
            }
            
            if (current_z == configuration_space_.depth() - 1) {
                // 0
                if (configuration_space.get(current_x,current_y,0) == search_value) {
                    current_z = 0;
                    continue;
                }
            } else {
                // обычное дело
                if (configuration_space.get(current_x,current_y,current_z+1) == search_value) {
                    ++current_z;
                    continue;
                }
            }
        }
        answer_in_cs_domain.emplace_back(current_x, current_y, current_z);
        //Преобразовать ответ
        std::vector<MapConfiguration<double>> answer;
        
        for (auto it = answer_in_cs_domain.crbegin(); it != answer_in_cs_domain.crend(); ++it) {
            //std::cout << get_angle_from_z_index(it->z()) << std::endl;
            answer.emplace_back( Point{get_x_position(it->x()),
                                       get_y_position(it->y())},
                                 get_angle_from_z_index(it->z()));
        }
        return move(answer);
    }
};

#endif
