//
//  main.cpp
//  Boost xml parser
//
//  Created by Alnen on 01.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <vector>
#include <utility>
#include <algorithm>
#include <chrono>
#include <ros/ros.h>

#include "Core.h"

#include "Point2D.h"
#include "Footprint.h"
#include "PathPlanner.h"
#include "RobotInfo.h"

using std::string;
using std::is_integral;
using std::is_floating_point;
using std::move;
using std::common_type;
using std::enable_if;

#include "Point2D.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

void test3() {
    boost::property_tree::ptree pt;
    read_xml("/Users/alnen/catkin_ws/src/mybot_mymovebase/settings.xml", pt);
    Core<double, int> core(pt);
    ROS_INFO("Initialized");
    core.execute();
    
    std::cout << "OK" << std::endl;
    
}

int main(int argc, char * argv[]) {
    ros::init(argc, argv, "simple_navigation_goals");
    test3();
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}
