//
//  Square Collision.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Square_Collision_h
#define Boost_xml_parser_Square_Collision_h

#include <limits.h>
#include <vector>

#include "Point2D.h"


using std::numeric_limits;

template <class P = double>
class SquareCollision
{
    using  Point = Point2D<P>;
    
private:
    Point     left_bottom_point_;
    Point     right_top_point_;
    
public:
    template <class Container = std::vector<Point2D<double>>>
    SquareCollision(const Container& points)
    {
        P x_min = numeric_limits<P>::max(),
          x_max = numeric_limits<P>::lowest(),
          y_min = numeric_limits<P>::max(),
          y_max = numeric_limits<P>::lowest();
        
        for (const Point& point : points){
            if (x_min > point.x()) x_min = point.x();
            if (x_max < point.x()) x_max = point.x();
            if (y_min > point.y()) y_min = point.y();
            if (y_max < point.y()) y_max = point.y();
        }
        left_bottom_point_.set_x(x_min);
        left_bottom_point_.set_y(y_min);
        
        right_top_point_.set_x(x_max);
        right_top_point_.set_y(y_max);
        
    }
    
    Point left_bottom_point() const { return left_bottom_point_; }
    Point right_top_point()   const { return right_top_point_;   }
    
    P bottom() const { return left_bottom_point_.y(); }
    P top()    const { return right_top_point_.y(); }
    P left()   const { return left_bottom_point_.x(); }
    P right()  const { return right_top_point_.x(); }
    
};

#endif
