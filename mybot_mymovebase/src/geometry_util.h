//
//  Header.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_geometry_util_h
#define Boost_xml_parser_geometry_util_h

#include <math.h>

#include "Point2D.h"

template <class P>
inline P  get_angle_from_3_points_0_2pi (const Point2D<P>& p1, const Point2D<P>& p2, const Point2D<P>& p3) {
    Vector2D<P> ab = { p2.x() - p1.x(), p2.y() - p1.y() };
    Vector2D<P> cb = { p2.x() - p3.x(), p2.y() - p3.y() };
    
    P dot = (ab.x() * cb.x() + ab.y() * cb.y());
    P cross = (ab.x() * cb.y() - ab.y() * cb.x());
    
    P alpha = atan2(cross, dot);
    
    return alpha<0.0 ? 2.0*M_PI+alpha : alpha;
}

template <class P>
inline P  get_angle_from_3_points_mpi_pi (const Point2D<P>& p1, const Point2D<P>& p2, const Point2D<P>& p3) {
    Vector2D<P> ab = { p2.x() - p1.x(), p2.y() - p1.y() };
    Vector2D<P> cb = { p2.x() - p3.x(), p2.y() - p3.y() };
    
    P dot = (ab.x() * cb.x() + ab.y() * cb.y());
    P cross = (ab.x() * cb.y() - ab.y() * cb.x());
    
    P alpha = atan2(cross, dot);
    
    return alpha<0.0 ? 2.0*M_PI+alpha : alpha;
}


#endif
