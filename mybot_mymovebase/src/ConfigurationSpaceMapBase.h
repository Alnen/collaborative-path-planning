//
//  Header.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_ConfigurationSpaceMapBase_h
#define Boost_xml_parser_ConfigurationSpaceMapBase_h

#include <utility>
#include <vector>

template <typename T,typename U = int>
class ConfigurationSpaceMapBase
{
    static_assert(std::is_integral<T>::value,"T must be of integral type");
    static_assert(std::is_integral<U>::value,"U must be of integral type");
 
public:
    using index_type = U;
    using map_value_type = T;
    
    ConfigurationSpaceMapBase(U width, U height, U depth)
    :map_(width*height*depth,T(0)),
    width_(width),
    height_(height),
    depth_(depth)
    {
        
    }
    
    ConfigurationSpaceMapBase (const ConfigurationSpaceMapBase& other) = default;
    
    index_type width()  const { return  width_; };
    index_type height() const { return height_; };
    index_type depth()  const { return  depth_; };
    
    map_value_type get (index_type x,index_type y,index_type z) const {
        return map_[x + y*width_ + z*height_*width_];
    }
    
    void set (index_type x,index_type y,index_type z,map_value_type value) {
        map_[x + y*width_ + z*height_*width_] = value;
    }
    
private:
    std::vector<map_value_type> map_;
    const index_type width_;
    const index_type height_;
    const index_type depth_;
    
};

using ConfigurationSpaceMap = ConfigurationSpaceMapBase<int>;

#endif
