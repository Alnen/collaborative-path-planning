//
//  Square Collision.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_TaskInfo_h
#define Boost_xml_parser_TaskInfo_h


enum class TaskStatus
{
    PROCESSING,
    WAITING
};


class TaskInfo
{
public:
    TaskInfo():
        status_(TaskStatus::WAITING),
        robot1_progress_(0),
        robot2_progress_(0)
    {

    }

    TaskStatus status()const{ return status_; }
    TaskStatus set_status(TaskStatus status){ status_= status; }


private:
    TaskStatus   status_;
    unsigned int robot1_progress_;
    unsigned int robot2_progress_;
};

#endif
