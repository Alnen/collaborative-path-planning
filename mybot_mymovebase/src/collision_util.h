//
//  Header.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Header_h
#define Boost_xml_parser_Header_h

#include <iostream>

#include "CircleCollision.h"
#include "SquareCollision.h"

template<class T, class U>
inline bool IsIntersecting (const CircleCollision<T>& circle1, const CircleCollision<U>& circle2) {
    return (circle1.center_of_mass_() - circle2.center_of_mass_()) > (circle1.radius() + circle2.radius());
}

template<class T, class U>
inline bool IsIntersecting (const SquareCollision<T>& square1, const SquareCollision<U>& square2) {
    return !(square1.bottom() > square2.top()||
            square1.top() < square2.bottom() ||
            square1.left() > square2.right() ||
            square1.right() < square2.left());
}

template<class T, class U, class P = typename common_type<T,U>::type>
inline bool IsIntersecting (const Point2D<T>& p11,
                     const Point2D<T>& p12,
                     const Point2D<U>& p21,
                     const Point2D<U>& p22)
{
    P denominator = ((p12.x() - p11.x()) * (p22.y() - p21.y())) - ((p12.y() - p11.y()) * (p22.x() - p21.x()));
    P numerator1  = ((p11.y() - p21.y()) * (p22.x() - p21.x())) - ((p11.x() - p21.x()) * (p22.y() - p21.y()));
    P numerator2  = ((p11.y() - p21.y()) * (p12.x() - p11.x())) - ((p11.x() - p21.x()) * (p12.y() - p11.y()));
    
    if (denominator == 0) return numerator1 == 0 && numerator2 == 0;
    
    P r = numerator1 / denominator;
    P s = numerator2 / denominator;
    
    return (r >= 0.0 && r <= 1.0) && (s >= 0.0 && s <= 1.0);
}

#endif
