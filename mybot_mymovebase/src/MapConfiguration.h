//
//  Configuration.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_MapConfiguration_h
#define Boost_xml_parser_MapConfiguration_h

#include "Point2D.h"

template <typename P, typename V = P>
class MapConfiguration
{
    static_assert(std::is_floating_point<V>::value,"");
    static_assert(std::is_floating_point<P>::value,"");
public:
    using point_precision_type = P;
    using angle_type = V;
    
    MapConfiguration(P x, P y,V angle):location_{x, y},angle_(angle) {}
    MapConfiguration(const Point2D<P>& point,V angle):location_(point),angle_(angle) {}
    
    const Point2D<P>& location() const { return location_; }
    V angle () const { return angle_; }


    void set_angle (V angle) { angle_ = angle; }
    void set_x (P x) { location_.set_x(x); }
    void set_y (P y) { location_.set_y(y); }
    
private:
    Point2D<P> location_;
    V          angle_;
};
#endif
