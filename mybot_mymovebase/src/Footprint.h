//
//  Footprint.h
//  Boost xml parser
//
//  Created by Alnen on 02.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Footprint_h
#define Boost_xml_parser_Footprint_h

#include <exception>
#include <iostream>
#include <math.h>
#include <utility>

#include "collision_util.h"
#include "Point2D.h"

template <class P = double,
          class SimplifiedCollisionForm = SquareCollision<P>,
          class Container = std::vector<Point2D<P>>
         >
class Footprint
{
    static_assert(std::is_integral<P>::value || std::is_floating_point<P>::value,"");
public:
    using  Precision = P;
    using  Point = Point2D<P>;
    using  CollisionForm = SimplifiedCollisionForm;
    
private:
    Container     points_;
    CollisionForm collision_form_;
    
public:
    Footprint(const Container& points):points_(points),collision_form_(points_) { }
    Footprint(Container&& points):points_(points),collision_form_(points_) { }
    
    Footprint(const Footprint& footprint) = default;
    Footprint(Footprint&& footprint) = default;
    Footprint& operator=(const Footprint& footprint) = default;
    
    template <class V>
    Footprint rotate(V angle,Point point = Point{0,0}) const {
        static_assert(is_floating_point<V>::value,"Angle must be of floating point type");

        Container temp = points_;
        
        P sin_theta = sin(angle);
        P cos_theta = cos(angle);
        
        if (point.x() == P(0) && point.y() == P(0)) {
            for (Point2D<P>& point: temp) {
                point = Point{ point.x()*cos_theta - point.y()*sin_theta, point.x()*sin_theta + point.y()*cos_theta };
            }
        } else {
            throw std::runtime_error("Dont do that bro");
        }
        return Footprint(move(temp));
    }
    
    const CollisionForm& collision_form() const { return collision_form_; }
    
    template <class V>
    Footprint translate(const Vector2D<V>& offset) const {
        Container temp = points_;
        
        if (offset.x() != V(0) || offset.y() != V(0)) {
            for (Point2D<P>& point: temp) {
                point += offset;
            }
        } /*else {
            throw std::runtime_error("Dont do that bro");
        }*/
        return Footprint(move(temp));
    }
    
    const Container& points() const { return points_; }
    
    template <class P1,class SCF, class C1>
    bool isColision(const Footprint<P1,SCF,C1>& footprint) const {
        if (IsIntersecting(collision_form_, footprint.collision_form())) {
            Point p11 = *points_.begin(),p12;
            Point p21, p22;
            bool flip1 = false,flip2 = false;
            
            for (auto it = ++points_.begin(); it != points_.end(); ++it) {
                if (flip1)
                    p11 = *it;
                else
                    p12 = *it;
                
                p21 = *footprint.points().begin();
                flip2 = false;
                
                for (auto it2 = ++footprint.points().begin(); it2 != footprint.points().end(); ++it2) {
                    if (flip2)
                        p21 = *it2;
                    else
                        p22 = *it2;
                    
                    if (IsIntersecting(p11,p12,p21,p22)) return true;
                    
                    flip2=!flip2;
                }
                if (flip2)
                    p21 = *footprint.points().begin();
                else
                    p22 = *footprint.points().begin();
                
                if (IsIntersecting(p11,p12,p21,p22)) return true;
                
                flip1=!flip1;
            }
            if (flip1)
                p11 = *points_.begin();
            else
                p12 = *points_.begin();
            
            p21 = *footprint.points().begin();
            flip2 = false;
            
            for (auto it2 = ++footprint.points().begin(); it2 != footprint.points().end(); ++it2) {
                if (flip2)
                    p21 = *it2;
                else
                    p22 = *it2;
                
                if (IsIntersecting(p11,p12,p21,p22)) return true;
                
                flip2=!flip2;
            }
            if (flip2)
                p21 = *footprint.points().begin();
            else
                p22 = *footprint.points().begin();
            
            if (IsIntersecting(p11,p12,p21,p22)) return true;
        }
        return false;
    }
    
    std::ostream& print (std::ostream& dc) const {
        for (const Point& point: points_) {
            dc << point.x() << " " << point.y() << std::endl;
        }
        dc <<"thats all" << std::endl;
        return dc;
    }
};

template <class P1,class SCF, class C1>
std::ostream& operator<<(std::ostream& dc, const Footprint<P1,SCF,C1>& footprint){
    return footprint.print(dc);
}



#endif
