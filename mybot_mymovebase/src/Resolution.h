//
//  Header.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Resolution_h
#define Boost_xml_parser_Resolution_h

#include <utility>

template <class P = double>
class Resolution
{
    static_assert(std::is_floating_point<P>::value,"");
private:
    P linear_resolution_;
    P angular_resolution_;
    
public:
    Resolution (P linear_resolution, P angular_resolution)
    :linear_resolution_(linear_resolution),
     angular_resolution_(angular_resolution) {}
    
    Resolution(const Resolution& other) = default;
    
    P linear_resolution()  const { return linear_resolution_; }
    P angular_resolution() const { return angular_resolution_; }
};

#endif
