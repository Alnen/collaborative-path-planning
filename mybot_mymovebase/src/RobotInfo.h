//
//  RobotInfo.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_RobotInfo_h
#define Boost_xml_parser_RobotInfo_h



#include <actionlib/client/simple_action_client.h>
#include <functional>
#include <move_base_msgs/MoveBaseAction.h>
#include <move_base_msgs/MoveBaseGoal.h>
#include <move_base_msgs/MoveBaseResult.h>
#include <vector>
#include <ros/ros.h>
#include <string>

#include "Footprint.h"
#include "geometry_util.h"
#include "MapConfiguration.h"
#include "Point2D.h"

using std::vector;


enum class PathStatus{
    WAITING_FOR_PATH,
    PROCEEDING,
    FINISHED
};

enum class ActionStatus{
    WAITING_FOR_ANSWER,
    NOTHING
};


template <class P = double,template <class> class SCF = SquareCollision>
class RobotInfo
{
public:
    template <class Container = std::vector<Footprint<P>>>
    RobotInfo(const Container&& footprint, const MapConfiguration<P>& location,
              const std::string& name, const std::string& topic_prefix,const std::string& tf_prefix)
    :colison_info_(footprint),
    last_known_configuration_(location),
    topic_prefix_(topic_prefix),
    tf_prefix_(tf_prefix),
    name_(name),
    path_(),
    path_status_(PathStatus::WAITING_FOR_PATH),
    current_index_(0),
    max_permissionded_index_(0),
    robot_move_base_client_(topic_prefix_ + "move_base" , false)
    {
        
    }

    RobotInfo(RobotInfo&& other):colison_info_(std::move(other.colison_info_)),
        last_known_configuration_(other.last_known_configuration_),
        topic_prefix_(other.topic_prefix_),
        tf_prefix_(other.tf_prefix_),
        name_(other.name_),
        path_(other.path_),
        path_status_(other.path_status_),
        current_index_(other.current_index_),
        max_permissionded_index_(other.max_permissionded_index_),
        robot_move_base_client_(topic_prefix_ + "move_base" , false)
    {

    }
    
    const Footprint<P,SCF<P>>& footprint() const { return colison_info_; }
    const MapConfiguration<P>& location() const  { return last_known_configuration_; }
    const std::string& tf_prefix() const { return tf_prefix_; }
    const std::string& topic_prefix() const { return topic_prefix_; }
    

    void set_new_path (vector<Point2D<P>>&& path)
    {
        path_ = path;
        current_index_= 0;
        max_permissionded_index_ = 0;
        current_command_is_rotation = true;
        if (path_.size() != 0){
            action_status_ = ActionStatus::NOTHING;
            path_status_ = PathStatus::PROCEEDING;
        }
    }

    void clear_path () {
        path_.clear();
        current_index_= 0;
        max_permissionded_index_ = 0;
        current_command_is_rotation = true;
        if (path_.size() != 0){
            action_status_ = ActionStatus::NOTHING;
            path_status_ = PathStatus::WAITING_FOR_PATH;
        }
    }

    size_t get_number_of_not_executed_commands () const
    {
        return max_permissionded_index_ - current_index_;
    }

    void increment_max_permision_index (size_t increment = 1) {
        max_permissionded_index_ += increment;
    }

    bool try_move () {
        if (get_number_of_not_executed_commands() != 0 && current_index_ + 1 < path_.size()){
            P new_angle = get_angle_from_3_points_0_2pi( Point2D<P>{1.0, 0.0}, Point2D<P>{0.0, 0.0}, path_[current_index_ + 1] - last_known_configuration_.location());
            if (current_command_is_rotation) {
                move_base_msgs::MoveBaseGoal rotate_goal;

                rotate_goal.target_pose.header.frame_id = "map";
                rotate_goal.target_pose.header.stamp    = ros::Time::now();

                rotate_goal.target_pose.pose.position.x = last_known_configuration_.location().x();
                rotate_goal.target_pose.pose.position.y = last_known_configuration_.location().y();

                rotate_goal.target_pose.pose.orientation.w = cos(new_angle/2);
                rotate_goal.target_pose.pose.orientation.z = sin(new_angle/2);
                ROS_INFO("%s before: %f %f %f \n",name_.c_str(),last_known_configuration_.location().x(),last_known_configuration_.location().y(),last_known_configuration_.angle());
                ROS_INFO("%s next: %f %f %f %f %f \n",name_.c_str(),
                         rotate_goal.target_pose.pose.position.x,
                         rotate_goal.target_pose.pose.position.y,
                         rotate_goal.target_pose.pose.orientation.z,
                         rotate_goal.target_pose.pose.orientation.w,
                         new_angle);

                robot_move_base_client_.sendGoal(rotate_goal, std::bind(&RobotInfo::action_result_callback, this, std::placeholders::_1, std::placeholders::_2));

            } else {
                move_base_msgs::MoveBaseGoal move_goal;

                move_goal.target_pose.header.frame_id = "map";
                move_goal.target_pose.header.stamp    = ros::Time::now();

                move_goal.target_pose.pose.position.x = path_[current_index_+1].x();
                move_goal.target_pose.pose.position.y = path_[current_index_+1].y();

                move_goal.target_pose.pose.orientation.w = cos(new_angle/2);
                move_goal.target_pose.pose.orientation.z = sin(new_angle/2);
                ROS_INFO("%s before: %f %f %f \n",name_.c_str(),last_known_configuration_.location().x(),last_known_configuration_.location().y(),last_known_configuration_.angle());
                ROS_INFO("%s next: %f %f %f \n",name_.c_str(),
                         move_goal.target_pose.pose.position.x,
                         move_goal.target_pose.pose.position.y,
                         move_goal.target_pose.pose.orientation.w);

                robot_move_base_client_.sendGoal(move_goal, std::bind(&RobotInfo::action_result_callback, this, std::placeholders::_1, std::placeholders::_2));
            }
            action_status_ = ActionStatus::WAITING_FOR_ANSWER;
            return true;
        } else {
            return false;
        }
    }

    actionlib::SimpleClientGoalState task_state() { return robot_move_base_client_.getState();}

    bool isDone () const { return current_index_ + 1 == path_.size()  || path_.size() == 0;}

    PathStatus path_status () const { return path_status_; }
    ActionStatus action_status () const { return action_status_; }

private:
    void action_result_callback(const actionlib::SimpleClientGoalState& state,const move_base_msgs::MoveBaseResult::ConstPtr& result){
        ROS_INFO("%s command procceded\n",name_.c_str());
        if(current_command_is_rotation){
            //rotate
            P new_angle = get_angle_from_3_points_0_2pi( Point2D<P>{1.0, 0.0}, Point2D<P>{0.0, 0.0}, path_[current_index_ + 1] - last_known_configuration_.location());
            last_known_configuration_.set_angle(new_angle);
        } else {
            //move
            last_known_configuration_.set_x(path_[current_index_+1].x());
            last_known_configuration_.set_y(path_[current_index_+1].y());
            ++current_index_;
            if (current_index_ + 1 == path_.size() ) path_status_ = PathStatus::FINISHED;
        }
        action_status_ = ActionStatus::NOTHING;
        current_command_is_rotation = !current_command_is_rotation;
    }

private:
    using Point = Point2D<P>;
    using Vector = Vector2D<P>;
    using MoveBaseClient = typename actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>;

    Footprint<P,SCF<P>> colison_info_;
    MapConfiguration<P> last_known_configuration_;

    std::string       topic_prefix_;
    std::string       tf_prefix_;
    std::string       name_;

    vector<Point>     path_;
    PathStatus        path_status_;
    ActionStatus      action_status_;
    size_t            current_index_;
    size_t            max_permissionded_index_;
    bool              current_command_is_rotation;

    MoveBaseClient    robot_move_base_client_;
};

#endif
