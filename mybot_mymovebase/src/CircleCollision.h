//
//  CircleCollision.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_CircleCollision_h
#define Boost_xml_parser_CircleCollision_h

#include "Point2D.h"

template <class P = double>
class CircleCollision
{
    using  Point = Point2D<P>;
    
private:
    P         radius_;
    Point     center_of_mass_;
    
public:
    template <class Container = std::vector<Point2D<P>>>
    CircleCollision(const Container& points)
    {
        Point center_of_mass;
        for (const Point& point : points){
            center_of_mass += point;
        }
        
        center_of_mass /= points.size();
        center_of_mass_ = center_of_mass;
        
        P radius = -1;
        P temp = -1;
        
        for (const Point& point : points){
            temp = (center_of_mass_ - point).norm();
            if ( temp > radius ) radius = temp;
        }
        radius_ = radius;
    }
    
    P     radius()         const { return radius_; }
    Point center_of_mass() const { return center_of_mass_;}
    
};


#endif
