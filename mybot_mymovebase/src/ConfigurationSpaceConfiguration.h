//
//  Configuration.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_ConfigurationSpaceConfiguration_h
#define Boost_xml_parser_ConfigurationSpaceConfiguration_h

#include <utility>

template <typename I>
class ConfigurationSpaceConfiguration
{
    static_assert(std::is_integral<I>::value,"");
    
 public:
    using index_type = I;
    
    ConfigurationSpaceConfiguration(index_type x, index_type y, index_type z):x_(x),y_(y),z_(z) {}
    
    index_type x() const { return x_; }
    index_type y() const { return y_; }
    index_type z() const { return z_; }
    
private:
    index_type x_;
    index_type y_;
    index_type z_;
};

#endif
