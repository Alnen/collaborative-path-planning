//
//  Core.h
//  Boost xml parser
//
//  Created by Alnen on 06.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Core_h
#define Boost_xml_parser_Core_h

#include <actionlib/client/simple_action_client.h>
#include <functional>
#include <move_base_msgs/MoveBaseAction.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <fstream>
#include <geometry_msgs/Twist.h>
#include <istream>
#include <ros/ros.h>
#include <string>
#include <utility>

#include "CircleCollision.h"
#include "geometry_util.h"
#include "Map.h"
#include "PathPlanner.h"
#include "RobotInfo.h"
#include "Resolution.h"
#include "SeparateUnifiedPath.h"
#include "TaskInfo.h"

using boost::property_tree::ptree;

template <class P, class MV>
class Core
{
    static_assert(std::is_integral<MV>::value,"MV must be integral");
    static_assert(std::is_floating_point<P>::value,"P must be of floating point type");
public:
    using map_value_type = MV;
    using floating_point_precision = P;
    
    Core(const ptree& ptree):
    robot1_info_(std::move(initializeRobotInfo<CircleCollision>(ptree, 0))),
    robot2_info_(std::move(initializeRobotInfo<CircleCollision>(ptree, 1))),
    map_info_(initializeMap(ptree)),
    resolution_info_(initializeResolution(ptree)),
    path_planner_(
              move(initializeObstacles(ptree)),
              map_info_,
              resolution_info_,
              getUniFootprint(robot1_info_, robot2_info_)
              ),
    path_seporator_((robot1_info_.location().location() - robot2_info_.location().location()).norm()),
    global_task_info_(TaskStatus::WAITING),
    subscribed_topic_(node_handler_.subscribe("go_to_configuration", 10, &Core::new_task_callback, this))
    {

    }
    
    void new_task_callback(const geometry_msgs::Twist& configuration){
        ROS_INFO("Recieved task");
        if (global_task_info_.status() == TaskStatus::WAITING){
            global_task_info_.set_status(TaskStatus::PROCESSING);
            ROS_INFO("Accepted it");

            MapConfiguration<P> goal_location{ Point{configuration.linear.x, configuration.linear.y}, configuration.angular.z};

            P current_angle = get_angle_from_3_points_0_2pi(Point{1.0,0.0}, Point{0.0,0.0}, robot2_info_.location().location() - robot1_info_.location().location());
            MapConfiguration<P>  current_location{robot1_info_.location().location(), current_angle};
            std::vector<MapConfiguration<P>> unified_path = std::move(path_planner_.getPath(current_location, goal_location));

            if(unified_path.size()==0);//TODO no path

            typename SeparateUnifiedPath<P>::Pair paths = std::move(path_seporator_(unified_path));

            robot1_info_.set_new_path(move(paths.first));
            robot2_info_.set_new_path(move(paths.second));

            robot1_info_.try_move();
            robot2_info_.try_move();
        }
    }





    void execute () {
        ros::Rate rate(10);
        while (ros::ok())
        {

            if (robot1_info_.path_status() == PathStatus::WAITING_FOR_PATH && robot2_info_.path_status() == PathStatus::WAITING_FOR_PATH) {
                //sleep
            } else if (robot1_info_.path_status() == PathStatus::FINISHED && robot2_info_.path_status() == PathStatus::FINISHED) {
                robot1_info_.clear_path();
                robot2_info_.clear_path();
            } else {

                bool robot1_ready_to_go = false;
                bool robot2_ready_to_go = false;

                if ( robot1_info_.path_status() == PathStatus::PROCEEDING && robot1_info_.get_number_of_not_executed_commands() == 0 ) robot1_ready_to_go = true;
                if ( robot2_info_.path_status() == PathStatus::PROCEEDING && robot2_info_.get_number_of_not_executed_commands() == 0 ) robot2_ready_to_go = true;

                if (robot1_ready_to_go && robot2_ready_to_go) {
                    robot1_info_.increment_max_permision_index();
                    robot2_info_.increment_max_permision_index();

                    robot1_info_.try_move();
                    robot2_info_.try_move();
                } else {
                    //one of them ready
                    if (robot1_info_.path_status() == PathStatus::PROCEEDING && !robot1_ready_to_go  && robot1_info_.task_state().isDone()) robot1_info_.try_move();
                    if (robot2_info_.path_status() == PathStatus::PROCEEDING && !robot2_ready_to_go  && robot2_info_.task_state().isDone()) robot2_info_.try_move();
                }
            }

            ros::spinOnce();
            rate.sleep();
        }
    }


    
private:
    
    template <template <class> class SCF>
    static RobotInfo<P,SCF>                 initializeRobotInfo     (const ptree& pt, int i);
    static std::vector<std::string>         get_array_from_attribute(const ptree& pt, const std::string& attribute_name,int size = -1);
    static size_t                           getNumberOfRobots       (const ptree& pt);
    static Map<P>                           initializeMap           (const ptree& pt);
    static Resolution<P>                    initializeResolution    (const ptree& pt);
    static std::vector<Footprint<P>>        initializeObstacles     (const ptree& pt);
    static Footprint<P,SquareCollision<P>>  getUniFootprint         (const RobotInfo<P,CircleCollision>& robot_info1, const RobotInfo<P,CircleCollision>& robot_info2);

private:
    using Point  = Point2D<P>;
    using Vector = Vector2D<P>;
    using MoveBaseClient = actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> ;
    
private:
    RobotInfo<P,CircleCollision>       robot1_info_;
    RobotInfo<P,CircleCollision>       robot2_info_;
    
    Map<P>                             map_info_;
    Resolution<P>                      resolution_info_;
    
    PathPlanner<MV,P,P>                path_planner_;
    SeparateUnifiedPath<P>             path_seporator_;

    TaskInfo                           global_task_info_;


    //ROS
    ros::NodeHandle                    node_handler_;
    ros::Subscriber                    subscribed_topic_;
};

template <class P, class MV>
template <template <class> class SCF>
RobotInfo<P,SCF>                 Core<P,MV>::initializeRobotInfo     (const ptree& pt, int i) {
    size_t count = 0;
    for (const auto& v : pt.get_child("robots")) {
        if (v.first == "robot") {
            if (count == i) {
                //string stuff
                std::string name         = v.second.get<std::string>("name.<xmlattr>.value");
                std::string topic_prefix = v.second.get<std::string>("topic_prfix.<xmlattr>.value");
                std::string tf_prfix     = v.second.get<std::string>("tf_prefix.<xmlattr>.value");
                //position stuff
                //xyz
                std::vector<std::string> xyz_tokens = std::move(get_array_from_attribute(v.second, "position.<xmlattr>.xyz", 3));
                //angle
                std::vector<std::string> angle_tokens = std::move(get_array_from_attribute(v.second, "position.<xmlattr>.rpy", 3));
                //footprint
                std::vector<Point2D<P>> points;
                for (const auto& footprint_child : v.second.get_child("footprint")) {
                    if (footprint_child.first == "point") {
                        std::vector<std::string> points_ = std::move(get_array_from_attribute(footprint_child.second, "<xmlattr>.xyz", 2));
                        points.emplace_back(std::stod(points_[0]), std::stod(points_[1]));
                    }
                }
                //
                return std::move(RobotInfo<P, SCF>(std::move(points),
                                                MapConfiguration<P>(std::stod(xyz_tokens[0]), std::stod(xyz_tokens[1]), std::stod(angle_tokens[2])),
                                                name,
                                                topic_prefix,
                                                tf_prfix)
                                    );
            } else
                    ++count;
        }
    }
    throw std::runtime_error("bad xml");
}

template <class P, class MV>
inline std::vector<std::string> Core<P,MV>::get_array_from_attribute (const ptree& pt, const std::string& attribute_name, int size) {
    std::string str_attribute = pt.get<std::string>(attribute_name);
    std::istringstream attribute_iss(str_attribute);
    std::vector<std::string> attribute_tokens{std::istream_iterator<std::string>{attribute_iss},
        std::istream_iterator<std::string>{}};
    if (size == -1) return std::move(attribute_tokens);
    if (attribute_tokens.size() != size) {
        throw std::runtime_error("bad xml ");
    }
    return std::move(attribute_tokens);
}

template <class P, class MV>
inline size_t Core<P,MV>::getNumberOfRobots (const ptree& pt) {
    size_t count = 0;
    for (const auto& v : pt.get_child("robots")) {
        if (v.first == "robot") ++count;
    }
    return count;
}


template <class P, class MV>
inline Map<P> Core<P,MV>::initializeMap(const ptree& pt){
    std::vector<std::string> left_bottom = get_array_from_attribute(pt, "map.<xmlattr>.left_bottom", 2);
    std::vector<std::string> size = get_array_from_attribute(pt, "map.<xmlattr>.size", 2);

    return Map<P>{ std::stod(left_bottom[0]),
                   std::stod(left_bottom[1]),
                   std::stod(size[0]),
                   std::stod(size[1])
                 };
}

template <class P, class MV>
inline Resolution<P> Core<P,MV>::initializeResolution(const ptree& pt){
    double linear = pt.get<double>("resolution.<xmlattr>.linear");
    int angular = pt.get<int>("resolution.<xmlattr>.angular");

    return Resolution<P>{ linear, 2.0*M_PI/angular};
}

template <class P, class MV>
std::vector<Footprint<P>> Core<P,MV>::initializeObstacles(const ptree& pt) {
    std::vector<Footprint<P>> obstacles;
    for (const auto& child: pt.get_child("world")) {
        if (child.first == "obstacle") {
            std::vector<Point2D<P>> points;
            std::vector<std::string> point_strs;
            for (const auto& point: child.second.get_child("")){
                if(point.first == "point"){
                    point_strs = get_array_from_attribute(point.second, "<xmlattr>.xyz", 2);
                    points.emplace_back(std::stod(point_strs[0]),std::stod(point_strs[1]));
                }
            }
            obstacles.emplace_back(points);
            points.clear();
        }
    }
    return std::move(obstacles);
}

template <class P, class MV>
Footprint<P,SquareCollision<P>> Core<P,MV>::getUniFootprint(
                                                            const RobotInfo<P,CircleCollision>& robot1_info,
                                                            const RobotInfo<P,CircleCollision>& robot2_info){
    std::vector<Point2D<P>> points;
    
    P radius = std::max(robot1_info.footprint().collision_form().radius()
                        , robot2_info.footprint().collision_form().radius());
    P distance = (robot1_info.location().location() - robot2_info.location().location()).norm();
    points.emplace_back(-radius, radius);
    points.emplace_back(distance+radius, radius);
    points.emplace_back(distance+radius, -radius);
    points.emplace_back(-radius, -radius);
    
    return std::move( Footprint<P,SquareCollision<P>>{std::move(points)});
}
#endif
