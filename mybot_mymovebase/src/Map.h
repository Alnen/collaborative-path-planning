//
//  Map.h
//  Boost xml parser
//
//  Created by Alnen on 03.04.15.
//  Copyright (c) 2015 Alnen. All rights reserved.
//

#ifndef Boost_xml_parser_Map_h
#define Boost_xml_parser_Map_h

#include <utility>

template<class P = double>
class Map
{
    static_assert(std::is_floating_point<P>::value || std::is_integral<P>::value,"");
private:
    P lx_;
    P ly_;
    P width_;
    P height_;
    
public:
    Map(P lx, P ly, P width, P height)
    :lx_(lx),ly_(ly),width_(width),height_(height) {}
    
    Map(const Map& other) = default;
    
    P lx()     const { return lx_; }
    P ly()     const { return ly_; }
    P width()  const { return width_; }
    P height() const { return height_; }
};
#endif
