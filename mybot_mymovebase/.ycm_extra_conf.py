import os
import ycm_core

def FlagsForFile(filename, **kwargs):
  flags = [
     '-Wall',
     '-std=c++11',
     '-stdlib=libc++',
     '-x',
     'c++',
     '-I',
     './src',

     '-I',
     '/opt/ros/indigo/include', 
     '-I',
     '/usr/include/x86_64-linux-gnu/c++/4.8/', 

     '-isystem',
     '/usr/include/c++/4.8.2'
     ]
  return {
    'flags': flags,
    'do_cache': True}
